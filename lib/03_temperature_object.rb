class Temperature
  def initialize(opts = {})
    default_opts = {:c => 0}
    opts = default_opts.merge opts
    @celcius = opts[:c] if opts.key?(:c) #Let's use celcius internally
    @celcius = Temperature.ftoc(opts[:f]) if opts.key?(:f)
  end

  def in_fahrenheit
    Temperature.ctof(@celcius)
  end

  def in_celsius
      @celcius
  end

  def self.ctof(c)
    c * 9.0/ 5 + 32
  end

  def self.ftoc(f)
    (f - 32) * 5.0 / 9
  end

  def self.from_celsius(c)
    Temperature.new(:c => c)
  end

  def self.from_fahrenheit(f)
    Temperature.new(:f => f)
  end

end

class Celsius < Temperature
  def initialize(temp)
    super(:c => temp)
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    super(:f => temp)
  end
end
