class Timer
    attr_accessor :seconds
    def initialize(seconds = 0)
        @seconds = seconds
    end
    def time_string
        hours = @seconds/(60*60)
        minutes = @seconds % (60*60) / 60
        seconds = @seconds % 60
        "#{padded(hours)}:#{padded(minutes)}:#{padded(seconds)}"
    end
    def padded(integer)
       "#{integer<10? 0 : ""}#{integer}"
    end

end
