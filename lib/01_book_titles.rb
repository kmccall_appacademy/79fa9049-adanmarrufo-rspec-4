DONT_CAPITALIZE = %w{the a an and if but or nor than how in on of}


class Book
  attr_reader :title
  def title=(value)
    value = value.capitalize
    @title = value.split.map do |word| 
      DONT_CAPITALIZE.include?(word)? word : word.capitalize
    end.join(" ")
  end
end
