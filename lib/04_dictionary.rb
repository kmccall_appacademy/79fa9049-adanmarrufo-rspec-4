class Dictionary

  attr_reader :entries

  def initialize(entries = {})
     @entries = entries
  end

  def keywords
      entries.keys.sort
  end

  def add(entries)
      if entries.is_a? Hash
          @entries.merge! entries
      elsif ! @entries.has_key? entries
        @entries[entries] = nil
      end
  end

  def include?(key)
     entries.has_key? key
  end

  def find(key_string)
     entries.select{|key,value|(key =~ /\A#{key_string}/) == 0}
  end

  def printable
      string = ""
      entries.keys.sort.each{|key| string << %Q|[#{key}] "#{entries[key]}"\n| }
      3.times{string.chomp!}
      string
  end
end
